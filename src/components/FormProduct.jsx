import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { productActions } from "../store/Product/slice";

const FormProduct = () => {
  const [formData, setFormData] = useState();
  const [formError, setFormError] = useState();
  const { studentEdit, studentSearch } = useSelector(
    (rootReducer) => rootReducer.productRedux
  );
  const [searchValue, setSearchValue] = useState();

  const dispatch = useDispatch();

  const handleForm = () => (event) => {
    const { name, value, minLength, max, validity, title } = event.target;

    // Validation
    let mess;
    if (minLength != -1 && !value.length) {
      mess = "Vui lòng nhập thông tin";
    } else if (value.length < minLength && minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} kí tự`;
      if (name == "phone") {
        mess = `Số điện thoại gồm đúng ${minLength} số`;
      }
    } else if (value.length > max && max) {
      mess = `Vui lòng nhập tối đa ${max} kí tự`;
      if (name == "phone") {
        mess = `Số điện thoại gồm ${max} số`;
      }
    } else if (validity.patternMismatch) {
      if (name == "ma" || name == "phone") {
        mess = `${title} chỉ gồm số`;
      } else if (name == "hoTen") {
        mess = `${title} chỉ bao gồm chữ`;
      } else if (name == "email") {
        mess = `Vui lòng nhập đúng định dạng ${title}`;
      }
    }

    setFormError({
      ...formError,
      [name]: mess,
    });

    setFormData({
      ...formData,
      [name]: mess ? undefined : value,
    });
  };
  const handleSearch = () => (event) => {
    const { value } = event.target;
    setSearchValue(value);
  };

  useEffect(() => {
    if (!studentEdit) return;
    setFormData(studentEdit);
  }, [studentEdit]);

  return (
    <div>
      <h4 className="bg-dark text-light text-left p-2 text-center">
        Thông tin sinh viên
      </h4>
      <form
        className="form-group mt-3"
        onSubmit={(event) => {
          event.preventDefault();

          const elements = document.querySelectorAll("input");
          let formError = {};
          elements.forEach((ele) => {
            const { name, value, minLength, max, validity, title } = ele;
            let mess;
            if (minLength != -1 && !value.length) {
              mess = "Vui lòng nhập thông tin";
            } else if (value.length < minLength && minLength) {
              mess = `Vui lòng nhập tối thiểu ${minLength} kí tự`;
              if (name == "phone") {
                mess = `Số điện thoại gồm đúng ${minLength} số`;
              }
            } else if (value.length > max && max) {
              mess = `Vui lòng nhập không quá ${max} kí tự`;
              if (name == "phone") {
                mess = `Số điện thoại gồm đúng ${max} số`;
              }
            } else if (validity.patternMismatch) {
              if (name == "ma" || name == "phone") {
                mess = `${title} chỉ gồm số`;
              } else if (name == "hoTen") {
                mess = `${title} chỉ bao gồm chữ`;
              } else if (name == "email") {
                mess = `Vui lòng nhập đúng định dạng ${title}`;
              }
            }
            formError[name] = mess;
          });

          let flag = false;
          for (let key in formError) {
            if (formError[key]) {
              flag = true;
              break;
            }
          }

          if (flag) {
            setFormError(formError);
            return;
          }

          if (studentEdit) {
            dispatch(productActions.updateStudent(formData));
          } else {
            dispatch(productActions.createStudent(formData));
          }
        }}
        noValidate
      >
        <div className="row">
          <div className="col-6">
            <label htmlFor="ma">Mã SV</label>
            <input
              value={formData?.ma}
              type="text"
              id="ma"
              className="form-control"
              name="ma"
              title="Mã sinh viên"
              onChange={handleForm()}
              minLength={1}
              max={6}
              pattern="^[0-9]+$"
              disabled={!!studentEdit}
            />
            <p className="text-danger">{formError?.ma}</p>
          </div>
          <div className="col-6">
            <label htmlFor="hoTen">Họ Tên</label>
            <input
              value={formData?.hoTen}
              type="text"
              id="hoTen"
              className="form-control"
              name="hoTen"
              onChange={handleForm()}
              minLength={1}
              pattern="[a-zA-ZÀ-ỹ\s]+"
              title="Họ và tên"
            />
            <p className="text-danger">{formError?.hoTen}</p>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-6">
            <label htmlFor="phone">Số điện thoại</label>
            <input
              value={formData?.phone}
              type="text"
              id="phone"
              className="form-control"
              name="phone"
              onChange={handleForm()}
              minLength={10}
              max={10}
              pattern="^[0-9]+$"
              title="Số điện thoại"
            />
            <p className="text-danger">{formError?.phone}</p>
          </div>
          <div className="col-6">
            <label htmlFor="email">Email</label>
            <input
              value={formData?.email}
              type="text"
              id="email"
              className="form-control"
              name="email"
              onChange={handleForm()}
              minLength={1}
              pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
              title="Email"
            />
            <p className="text-danger">{formError?.email}</p>
          </div>
        </div>
        <div className="mt-3 d-flex justify-content-between">
          <div>
            {!studentEdit && (
              <button className="btn btn-success">Thêm sinh viên</button>
            )}
            {studentEdit && (
              <button className="btn btn-warning ml-3">Sửa thông tin</button>
            )}
            <button type="reset" className="btn btn-danger ml-3">
              Xóa form
            </button>
          </div>
          <div>
            <input
              type="text"
              className="mr-2"
              placeholder="Số điện thoại"
              onChange={handleSearch()}
            />
            <button
              type="button"
              className="btn btn-info"
              onClick={() => {
                dispatch(productActions.searchStudent(searchValue));
              }}
            >
              Tìm Kiếm
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default FormProduct;
