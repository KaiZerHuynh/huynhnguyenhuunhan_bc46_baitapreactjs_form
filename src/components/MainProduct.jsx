import React from "react";
import FormProduct from "./FormProduct";
import TableProduct from "./TableProduct";

const MainProduct = () => {
  return (
    <div className="container">
      <FormProduct></FormProduct>
      <TableProduct></TableProduct>
    </div>
  );
};

export default MainProduct;
