import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { productActions } from "../store/Product/slice";

const TableProduct = () => {
  const { studentList, studentSearch } = useSelector(
    (rootReducer) => rootReducer.productRedux
  );
  const dispatch = useDispatch();
  return (
    <div>
      <table className="table">
        <thead className="bg-dark text-light">
          <tr>
            <td>Mã SV</td>
            <td>Họ tên</td>
            <td>SĐT</td>
            <td>Email</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          {(() => {
            let temp_list = [];
            if (studentSearch.length == 0) {
              temp_list = studentList;
            } else {
              temp_list = studentSearch;
            }
            return temp_list?.map((student) => (
              <tr key={student.ma}>
                <td>{student.ma}</td>
                <td>{student.hoTen}</td>
                <td>{student.phone}</td>
                <td>{student.email}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      dispatch(productActions.deleteStudent(student.ma));
                    }}
                  >
                    Xóa
                  </button>
                  <button
                    className="btn btn-info ml-2"
                    onClick={() => {
                      dispatch(productActions.editStudent(student));
                    }}
                  >
                    Sửa
                  </button>
                </td>
              </tr>
            ));
          })()}
        </tbody>
      </table>
    </div>
  );
};

export default TableProduct;
