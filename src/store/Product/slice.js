import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  studentList: [],
  studentEdit: undefined,
  studentSearch: [],
};

const productSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    createStudent: (state, action) => {
      state.studentList.push(action.payload);
    },
    deleteStudent: (state, action) => {
      state.studentList = state.studentList.filter(
        (student) => student.ma != action.payload
      );
    },
    editStudent: (state, action) => {
      state.studentEdit = action.payload;
    },
    updateStudent: (state, action) => {
      state.studentList = state.studentList.map((student) => {
        if (student.ma == action.payload.ma) {
          return action.payload;
        }
        return student;
      });
      state.studentEdit = undefined;
    },
    searchStudent: (state, action) => {
      if (!action.payload) {
        return;
      }
      state.studentSearch = state.studentList.filter((student) => {
        if (student.phone == action.payload) {
          return student;
        }
      });
    },
  },
});

export const { actions: productActions, reducer: productReducer } =
  productSlice;
