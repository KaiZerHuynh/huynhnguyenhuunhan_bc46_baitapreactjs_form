import { combineReducers } from "redux";
import { productReducer } from "./Product/slice";

export const rootReducer = combineReducers({
  productRedux: productReducer,
});
